package com.kevin.spring.practise;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kevin on 2017/1/8.
 */
@Configuration
@Aspect
@EnableAspectJAutoProxy
public class TrackCounter {

    private Map<Integer, Integer> trackCounts = new HashMap<>();

    @Pointcut("execution(* com.kevin.spring.practise.PlayTrack.play(int)) && args(trackNumber)")
    public void trackPlayed(int trackNumber) {}

    @Before("trackPlayed(trackNumber)")
    public void countTrack(int trackNumber) {
        final int playCount = getPlayCount(trackNumber);
        System.out.println("trackNumber: " + trackNumber + " played " + playCount + " times.");
        trackCounts.put(trackNumber, playCount+1);
    }

    private int getPlayCount(int trackNumber) {
        return trackCounts.containsKey(trackNumber) ? trackCounts.get(trackNumber) : 0;
    }

    public void pringCountResult(int trackNumber) {
        System.out.println("trackNumber: "+ trackNumber + " played " + trackCounts.get(trackNumber) + " times");
    }

}
