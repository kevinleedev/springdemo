package com.kevin.spring.practise;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

/**
 * Created by kevin on 2017/1/5.
 */
@Aspect
@Component
@EnableAspectJAutoProxy
public class RecordLog {

    @Pointcut("execution(* com.kevin.spring.practise.Performance.perform(..))")
    public void perform() {}

    @Pointcut("execution(* com.kevin.spring.practise.PlayTrack.play(..))")
    public void playTrackLog() {}

    @Before("playTrackLog()")
    public void doLog() {
        System.out.println("log it...");
    }

//    @Before("perform()")
//    public void log() {
//        System.out.println("people run...");
//    }

//    @Around("perform()")
//    public void log(ProceedingJoinPoint joinPoint) {
//        System.out.println("log start...");
//        try {
//            joinPoint.proceed();
//        } catch (Throwable throwable) {
//            throwable.printStackTrace();
//        }
//        System.out.println("log end...");
//
//    }

}
