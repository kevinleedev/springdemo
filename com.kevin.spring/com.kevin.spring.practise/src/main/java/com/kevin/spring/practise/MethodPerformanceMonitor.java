package com.kevin.spring.practise;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 * Created by kevin on 2016/12/29.
 */
public class MethodPerformanceMonitor {

//    @Around(value="execution(* com.kevin.spring.practise..*.*(..))")
    public Object permMonitor(ProceedingJoinPoint joinPoint) throws Throwable {
       long start = System.currentTimeMillis(      );
        Object ret = joinPoint.proceed();
        long end = System.currentTimeMillis();
        System.out.println("方法: "+joinPoint.getSignature().getName()+" 耗时：" + (end-start) + "毫秒");
        return ret;
    }

}
