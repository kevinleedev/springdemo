package com.kevin.spring.practise;

import org.springframework.context.annotation.Configuration;

/**
 * Created by kevin on 2017/1/8.
 */
@Configuration
public class PlayTrack {

    public void play(int trackNumber) {
        System.out.println("trackNumber: " + trackNumber + " is playing...");
    }

}
