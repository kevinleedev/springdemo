package com.kevin.spring.practise;

import org.springframework.context.annotation.Configuration;

/**
 * Created by kevin on 2017/1/5.
 */
@Configuration
public class Performance {

    public void perform() {
        System.out.println("perform...");
    }

}
