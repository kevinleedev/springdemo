package com.kevin.spring.practise.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by kevin on 2017/1/4.
 */
@Controller
@RequestMapping("/")
public class IndexController {

    private static Logger LOGGER = LoggerFactory.getLogger(IndexController.class);

    @RequestMapping
    public String index() {
        LOGGER.info("index page");
        return "index";
    }

}
