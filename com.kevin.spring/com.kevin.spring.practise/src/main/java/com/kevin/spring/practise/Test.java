package com.kevin.spring.practise;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Created by kevin on 2017/1/5.
 */
@EnableAspectJAutoProxy
@Configuration
@ComponentScan
public class Test {

    @Bean
    public static Performance getPerformance() {
        return new Performance();
    }

    public static void main(String[] args) {
//        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        getPerformance().perform();
    }

}
