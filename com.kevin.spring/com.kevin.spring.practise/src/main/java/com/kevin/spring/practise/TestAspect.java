package com.kevin.spring.practise;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * Created by kevin on 2017/1/8.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-config.xml"})
public class TestAspect {

    @Resource
    private TrackCounter trackCounter;
    @Resource
    private PlayTrack playTrack;
    @Resource
    private RecordLog recordLog;

    @org.junit.Test
    public void testCount() {
        int trackNumber = 1;
        playTrack.play(trackNumber);
        playTrack.play(trackNumber);
        playTrack.play(trackNumber);
        playTrack.play(trackNumber);

//        trackCounter.pringCountResult(trackNumber);
//        recordLog.playTrackLog();
    }

}
